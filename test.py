#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from cStringIO import StringIO
import re
import telegram
import os
import time
import imaplib
import email

bot = telegram.Bot(token='266072849:AAGWqB3gRniTzC2282doAh4OzLyGWgeagpw')
chat = -25809425
chat2 = -156055976

def getAttachmentFromEmail():
    try:
        #log in and select the inbox
        mail = imaplib.IMAP4_SSL('imap.gmail.com')
        mail.login('tester.norse@gmail.com', '321654qazwsx')
        mail.select('inbox')

        #get uids of all messages
        result, data = mail.uid('search', None, 'ALL')
        uids = data[0].split()

        #read the lastest message
        result, data = mail.uid('fetch', uids[-1], '(RFC822)')
        raw_email = data[0][1]
        raw_email_string = raw_email.decode('utf-8')
        m = email.message_from_string(raw_email_string)

        # #get message body
        # result, data = mail.search(None, '(UNSEEN SUBJECT "%s")' % subject)
        # for num in data[0].split():
        #     typ, data = conn.fetch(num, '(RFC822)')
        #     msg = email.message_from_string(data[0][1])
        #     typ, data = conn.store(num, '-FLAGS', '\\Seen')
        #     yield msg

        for part in m.walk():
            # print part.get_payload(decode=True)
            print part.get_content_type()
            if part.get_content_type() == "text/plain":
                message = part.get_payload(decode=True)
                bot.sendMessage(chat_id=chat2, text=message)
                print part.get_payload(decode=True)
            if m.get_content_maintype() == 'multipart':  # multipart messages only
                print part.get_payload()
                #find the attachment part
                if part.get_content_maintype() == 'multipart': continue
                if part.get('Content-Disposition') is None: continue

                #save the attachment in the program directory
                filename = part.get_filename()
                fp = open(filename, 'wb')
                fp.write(part.get_payload(decode=True))
                fp.close()
    except:
        pass
    # mail.store("1:*", '+X-GM-LABELS', '\\Trash')

def convert_pdf_to_txt(path):
    rsrcmgr = PDFResourceManager('1.pdf')
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = file('1.pdf', 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos=set()

    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text



def parse():
    f = convert_pdf_to_txt('1.pdf')
    r = unicode(f, 'utf-8')
    m = re.split(U'Виконані роботи:', r, maxsplit=1)
    first_m = m[0]
    second_m = re.sub(U'НАРЯД НА ОБСЛУГОВУВАННЯ №',' ', first_m)
    thrid_m = re.split(U'Замовлена дата та час обслуговування:', second_m, maxsplit=1)
    four_m = re.split(U'Замовник:', thrid_m[0], maxsplit=1)
    number = re.sub(r'\s', '', four_m[0])
    five_m = re.split(U'«', thrid_m[1], maxsplit=1)
    adres = five_m[0]
    date1 = re.split(U'Службова інформація:',five_m[1],maxsplit=1)
    date = date1[0]
    six_m = re.split(U'встановлення:', five_m[1], maxsplit=1)
    mdu1 = re.split(U'IP-адреса', six_m[1], maxsplit=1)
    mdu = mdu1[0]
    eight_m = re.split(U'Опис звернення:', six_m[1], maxsplit=1)
    message = eight_m[1]
    sub_date = re.sub(U'\n', '', date)
    sub_date1 = re.sub(U'»', '', sub_date)
    sub_date2 = re.sub('    ', ' ', sub_date1)
    sub_date3 = re.sub('    ', ' ', sub_date2)
    sub_date4 = re.sub('  ', ' ', sub_date3)
    fio = re.search(u'^(\W+)(\n)(\W+)\s(\W+)(\d+|\d+\W)(\n)(\d+|\d+\W)(\n)(\W+)(\n)(\d{10}),\s(\d{9})',adres)
    with open("numbers.txt", "a") as myfile:
        myfile.write("+38{0}".format(fio.group(11)).encode('utf-8')+", "+"+38{0}".format(fio.group(12)).encode('utf-8')+", "+fio.group(1).encode('utf-8').strip()+", "+fio.group(3).encode('utf-8')+" "+fio.group(4).encode('utf-8').strip()+" "+fio.group(5).encode('utf-8')+" "+fio.group(7).encode('utf-8')+", "+ fio.group(9).encode('utf-8')+"\n")
    bot.sendMessage(chat_id=chat2, text=fio.group(3)+" "+fio.group(4)+fio.group(5)+" "+fio.group(7)+"\n"+ fio.group(9)+"\n\n"+"<a href='tel:+3'>+38{0}</a>".format(fio.group(11))+" "+"<a href='tel:+3'>+380{0}</a>".format(fio.group(12))+mdu+number,
                    disable_web_page_preview=True, parse_mode='HTML')
    # bot.send_contact(chat_id=chat2, phone_number=phone, first_name="dd", last_name="dd")
    # os.remove('1.pdf')


parse()
