# -*- coding: utf-8 -*-
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, Job
import logging
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from cStringIO import StringIO
import re
import imaplib
import email
import os
from bs4 import BeautifulSoup
import urllib3
import time
from datetime import timedelta, datetime

date = time.strftime("%Y-%m-%d")

BASE_URL = 'http://194.44.229.102:4040/index.php?action=login&login=y.sashurin&password=5a2ed9c2d923dd3fa89278dde26a2f97'
BASE_URL2 = 'http://194.44.229.102:4040/headless.php?action=workschedule1&city=%D0%A7%D0%BE%D1%80%D0%BD%D0%BE%D0%BC%D0%BE%D1%80%D1%81%D1%8C%D0%BA&data={0}'.format(date)
START_TIME = datetime.now()

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

chat = -25809425
chat2 = -71046484
http = urllib3.PoolManager()

def get_html():
    response = http.request('GET', BASE_URL)
    cookie = response.headers.get('Set-Cookie')
    http.headers['cookie'] = cookie
    response2 = http.request('GET', BASE_URL2)
    return response2.data

def raw(html):
    soup = BeautifulSoup(html, 'html.parser')
    return soup.text


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    bot.sendMessage(update.message.chat_id, text='Hi!')

def ping(bot, update):
    uptime = datetime.now() - START_TIME
    bot.sendMessage(update.message.chat_id, text='Я на связи! Время непрерывной работы:  ' + str(uptime), parse_mode="HTML")

def plan(bot, update):
    bot.sendMessage(update.message.chat_id, text=raw(get_html()))


def echo(bot, update):
    bot.sendMessage(update.message.chat_id, text=update.message.text)


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))


def job1(bot, job):
    getAttachmentFromEmail(bot, job)

def getAttachmentFromEmail(bot, job):
    try:
        #log in and select the inbox
        mail = imaplib.IMAP4_SSL('imap.gmail.com')
        mail.login('tester.norse@gmail.com', '321654qazwsx')
        mail.select('inbox')

        #get uids of all messages
        result, data = mail.uid('search', None, 'ALL')
        uids = data[0].split()

        #read the lastest message
        result, data = mail.uid('fetch', uids[-1], '(RFC822)')
        m = email.message_from_string(data[0][1])


        for msg in m.walk():

                #find the attachment part
                if msg.get_content_maintype() == 'text': continue
                if msg.get('Content-Disposition') is None: continue
                if msg.get('X-Attachment-Id') is None: continue
                # print msg.get_content_type()
                # print msg.get('X-Attachment-Id')
                # print msg.get_filename()

                #save the attachment in the program directory
                filename = msg.get_filename()
                fp = open(filename, 'wb')
                fp.write(msg.get_payload(decode=True))
                fp.close()
                try:
                    parse(filename, bot, job)
                except:
                    continue
                mail.store("1:*", '+X-GM-LABELS', '\\Trash')
    except:
        pass


def convert_pdf_to_txt(filename):
    rsrcmgr = PDFResourceManager(filename)
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = file(filename, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos=set()

    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text



def parse(filename, bot, job):
    f = convert_pdf_to_txt(filename)
    r = unicode(f, 'utf-8')
    m = re.split(U'Виконані роботи:', r, maxsplit=1)
    first_m = m[0]
    second_m = re.sub(U'НАРЯД НА ОБСЛУГОВУВАННЯ №',' ', first_m)
    thrid_m = re.split(U'Замовлена дата та час обслуговування:', second_m, maxsplit=1)
    four_m = re.split(U'Замовник:', thrid_m[0], maxsplit=1)
    number = re.sub(r'\s', '', four_m[0])
    five_m = re.split(U'«', thrid_m[1], maxsplit=1)
    adres = five_m[0]
    date1 = re.split(U'Службова інформація:',five_m[1],maxsplit=1)
    date = date1[0]
    six_m = re.split(U'встановлення:', five_m[1], maxsplit=1)
    mdu1 = re.split(U'IP-адреса', six_m[1], maxsplit=1)
    mdu = mdu1[0]
    eight_m = re.split(U'Опис звернення:', six_m[1], maxsplit=1)
    message = eight_m[1]
    sub_date = re.sub(U'\n', '', date)
    sub_date1 = re.sub(U'»', '', sub_date)
    sub_date2 = re.sub('    ', ' ', sub_date1)
    sub_date3 = re.sub('    ', ' ', sub_date2)
    sub_date4 = re.sub('  ', ' ', sub_date3)
    bot.sendMessage(chat_id=chat,
                    text="<b>" + sub_date4 + "</b>" + '                                                                     ' + "<b>" + number + "</b>" + "<b>" + adres + "</b>" + mdu + message,
                    disable_web_page_preview=True, parse_mode="HTML")
    os.remove(filename)

def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater("152087785:AAES7jVmX_f7y2YbNGyYtmOIDf-cO81QG3o")
    j = updater.job_queue
    job_minute = Job(job1, 10.0)
    j.put(job_minute, next_t=0.0)
    # Get the dispatcher to register handlers
    dp = updater.dispatcher
    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("plan", plan))
    dp.add_handler(CommandHandler("ping", ping))


    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler([Filters.text], echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
